import React from 'react';
import './BillItem.css';

const BillItem = props => {
    const amount = props.item.amount;
    const price = props.price;
    const totalPriceForItem = amount * price;

    const billItem = (
        <div className="BillItemWrapper">
            <div className="BillItem">
                <div className="BillItemFirstCol">
                    <span className="BillItemTitle">{props.item.type}</span>
                    <span className="BillItemAmount"> x {props.item.amount}   </span>
                </div>

                <button className="deleteBillItemBtn" onClick={props.delete}><i className="fa fa-trash-o" aria-hidden="true"/></button>
                <span className="BillItemPrice">  ${totalPriceForItem}</span>

            </div>
        </div>
    );

    return amount > 0 ? billItem : null;


};

export default BillItem;