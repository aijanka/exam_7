import React from 'react';

const EmptyBill = () => {
    return (
        <div className="EmptyBill">
            <p>Order is empty!</p>
            <p>Please, add some items!</p>
        </div>
    )
};

export default EmptyBill;