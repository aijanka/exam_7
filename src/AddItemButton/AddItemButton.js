import React from 'react';
import './AddItemButton.css';

const AddItemButton = props => {
    const iconClass = props.isDrink ? 'fa fa-coffee' : 'fa fa-cutlery';
    return (
        <button className='AddItemButton' onClick={props.click}>
            <i className={iconClass} aria-hidden="true"/>
            <span className='AddItemButtonTitle'>{props.type}  </span>
            <span className="itemPrice">Price: ${props.price}</span>
        </button>
    )
};

export default AddItemButton;