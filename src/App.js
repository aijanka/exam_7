import React, {Component} from 'react';
import './App.css';
import AddItemButton from "./AddItemButton/AddItemButton";
import BillItem from './BillItems/BillItem';
import EmptyBill from "./BillItems/EmptyBill";
import './AddItemsField.css';
import './BillWrapper.css';

class App extends Component {
    state = {
        items: [
            {type: 'Hamburger', amount: 0, id: 'hh', isDrink: false},
            {type: 'Coffee', amount: 0, id: 'co', isDrink: true},
            {type: 'Cheeseburger', amount: 0, id: 'ch', isDrink: false},
            {type: 'Tea', amount: 0, id: 'tt', isDrink: true},
            {type: 'Fries', amount: 0, id: 'ff', isDrink: false},
            {type: 'Cola', amount: 0, id: 'cc', isDrink: true},
            {type: 'Shaurma', amount: 0, id: 'sh', isDrink: false},
            {type: 'Fanta', amount: 0, id: 'fa', isDrink: true},
            {type: 'Chocolate', amount: 0, id: 'cho', isDrink: false},
            {type: 'Cocktail', amount: 0, id: 'coc', isDrink: true}
        ]
    };

    prices = {
        Hamburger: 80,
        Cheeseburger: 90,
        Fries: 45,
        Coffee: 80,
        Tea: 20,
        Cola: 30,
        Shaurma: 100,
        Fanta: 30,
        Chocolate: 50,
        Cocktail: 200
    };

    addItem = type => {
        const items = [...this.state.items];
        const index = items.findIndex(element => element.type === type);
        items[index].amount++;
        this.setState({items});
        this.countTotalSum();
    };

    removeOneItem = type => {
        const items = [...this.state.items];
        const index = items.findIndex(element => element.type === type);
        items[index].amount = 0;
        this.setState({items});
    };

    countTotalSum = () => {
        let sum = 0;
        this.state.items.forEach(item => {
            sum += item.amount * this.prices[item.type];
        });
        return sum;
    };

    render() {
        return (
            <div className="App">
                <h1 className="Menu">Menu</h1>
                <div className="AddItemsWrapper">
                    <div className="AddItemsField">
                        {this.state.items.map(item => {
                            return (
                                <AddItemButton
                                    type={item.type}
                                    amount={item.amount}
                                    id={item.id}
                                    isDrink={item.isDrink}
                                    price={this.prices[item.type]}
                                    key={item.id}
                                    click={() => this.addItem(item.type)}
                                />
                            )
                        })}
                    </div>
                </div>

                <div className="BillWrapper">
                    <div className="Bill">
                        <p className='OrderTitle'>Order details</p>
                        {(this.countTotalSum() === 0) ? (
                            <EmptyBill/>
                        ) : (
                            this.state.items.map( item => {
                                return (<BillItem
                                    item={item}
                                    price={this.prices[item.type]}
                                    key={item.type}
                                    delete={() => this.removeOneItem(item.type)}
                                />)
                            })
                        )
                        }
                        <p className="total">Total price: $ {this.countTotalSum()}</p>
                    </div>
                </div>


            </div>
        );
    }
}

export default App;
